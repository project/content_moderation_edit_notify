<?php
// phpcs:ignoreFile
namespace Drupal\Tests\content_moderation_edit_notify\Kernel;

use Drupal\content_moderation_edit_notify\Controller\ContentModerationNotifyController;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * @coversDefaultClass \Drupal\content_moderation_edit_notify\Hook\NodeFormAlter
 * @group content_moderation_edit_notify
 */
class ContentModerationNotifyControllerKernelTest extends KernelTestBase {

  use NodeCreationTrait;
  use UserCreationTrait;
  use ContentModerationTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation_edit_notify',
    'content_moderation',
    'entity_test',
    'node',
    'filter',
    'system',
    'user',
    'workflows',
    'language',
    'content_translation',
  ];

  protected $config;
  protected $contentModerationNotifyController;
  protected $moderationInformation;
  protected $nodeStorage;
  protected $token;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('content_moderation_state');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installConfig(['content_moderation', 'filter', 'content_moderation_edit_notify']);
    $this->installSchema('system', 'sequences');
    $this->installSchema('node', ['node_access']);

    $this->config = $this->config('content_moderation_edit_notify.settings');
    $this->moderationInformation = $this->container->get('content_moderation.moderation_information');
    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');
    $this->token = $this->container->get('token');

    ConfigurableLanguage::createFromLangcode('fr')->save();

    $node_type = NodeType::create([
      'type' => 'example',
    ]);
    $node_type->save();

    DateFormat::create([
      'id' => 'fallback',
      'pattern' => 'D, m/d/Y - H:i',
    ])->save();

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'example');
    $workflow->save();

    $this->contentModerationNotifyController = new ContentModerationNotifyController(
      $this->moderationInformation,
      $this->token
    );

  }

  /**
   * Tests the check controller.
   */
  public function testCheck() {
    // Create node draft.
    $node = $this->createNode(['type' => 'example']);
    $node->save();

    $result = $this->contentModerationNotifyController->check($node, $node);

    $this->assertTrue($node->isDefaultRevision());
    $this->assertEquals($result, new AjaxResponse(), 'Same revision expect empty response');

    $nodePreviousRevision = $this->nodeStorage->loadRevision($node->getRevisionId());

    $node->setNewRevision();
    $node->setTitle('revised_dummy_title');
    $node->save();

    $result = $this->contentModerationNotifyController->check($node, $nodePreviousRevision);

    $this->assertTrue($node->isDefaultRevision());
    $this->assertResultCommandMatch($result, $node, 'message_unpublished', 'warning');

    $nodePreviousRevision = $this->nodeStorage->loadRevision($node->getRevisionId());

    // Set node published.
    $node->setNewRevision();
    $node->setTitle('revised_published_dummy_title');
    $node->moderation_state = 'published';
    $node->save();

    $result = $this->contentModerationNotifyController->check($node, $nodePreviousRevision);

    $this->assertTrue($node->isDefaultRevision());
    $this->assertResultCommandMatch($result, $node, 'message_published', 'error');

    $french_node = $node->addTranslation('fr', ['title' => 'French title']);
    $french_node->moderation_state->value = 'draft';
    $french_node->save();

    $result = $this->contentModerationNotifyController->check($french_node, $nodePreviousRevision);
    $this->assertEquals($result, new AjaxResponse(), 'Same revision expect empty response');
  }

  /**
   * Helper to check response message.
   */
  private function assertResultCommandMatch($result, $node, $messageConfigName, $messageType) {
    $result = $result->getCommands()[0];

    $this->assertEquals($result['command'], 'message', 'Result message contain our message');
    $this->assertEquals($result['messageOptions']['type'], $messageType, 'Result message is of proper type');

    $message = $this->token->replace($this->config->get($messageConfigName), ['node' => $node]);
    $this->assertEquals($result['message'], $message, 'Result message is what expected for published');
  }

}
